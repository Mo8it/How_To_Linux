mod day1;
mod day2;

use collective_score_client::{run, Tasks};
use std::process;

fn main() {
    let day1_tasks = day1::tasks();
    let day2_tasks = day2::tasks();

    let n_tasks = day1_tasks.len() + day2_tasks.len();

    let tasks: Tasks = day1_tasks.into_iter().chain(day2_tasks).collect();

    assert_eq!(tasks.len(), n_tasks, "Task name conflict!");

    if let Err(e) = run(
        tasks,
        "collective-score-dev-tools",
        "https://collective-score.mo8it.com",
    ) {
        eprintln!("{e:?}");
        process::exit(1);
    }
}
