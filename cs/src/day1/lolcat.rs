use collective_score_client::{
    check::{Check, IntoTask, Task},
    validator::{
        command::{Command, CommandStatus, StdioVariant},
        file::FileContent,
        string_content::StringContent,
    },
};
use regex::Regex;

pub fn task() -> Task {
    let fish_history = dirs::home_dir()
        .expect("Failed to get the home directory!")
        .join(".local/share/fish/fish_history");

    [
        Check::builder()
            .description("Checking that `lolcat` is installed")
            .validator(
                CommandStatus::builder()
                    .command(
                        Command::builder()
                            .program("which")
                            .args(vec!["lolcat".into()])
                            .stdout(StdioVariant::Null)
                            .stderr(StdioVariant::Null)
                            .stdin(StdioVariant::Null)
                            .build(),
                    )
                    .status_code(0)
                    .build(),
            )
            .hint("Did you install `lolcat` using the `dnf` package manager?")
            .build()
            .into_box(),
        Check::builder()
            .description("Checking that you piped `cowsay` into `lolcat`")
            .validator(FileContent {
                file: fish_history.clone(),
                expected: StringContent::Regex(
                    Regex::new(r"\n- cmd: cowsay .+ \| lolcat").expect("Failed to build a regex"),
                ),
            })
            .hint("Did you try the syntax with `|`?")
            .build()
            .into_box(),
        Check::builder()
            .description("Checking that you tried adding randomness to `lolcat`")
            .validator(FileContent {
                file: fish_history.clone(),
                expected: StringContent::Regex(
                    Regex::new(r"lolcat.* (-r( |\n)|--random)").expect("Failed to build a regex"),
                ),
            })
            .hint("Did you read the help message of `lolcat` to find out how to add randomness to the output colors?")
            .build()
            .into_box(),
    ]
    .into_task("lolcat")
}
