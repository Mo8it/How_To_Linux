use collective_score_client::{
    check::{Check, IntoTask, Task},
    validator::{file::FileContent, string_content::StringContent},
};

pub fn task() -> Task {
    Check::builder()
        .description("Checking that you tried to update the system")
        .validator(FileContent {
            file: dirs::home_dir()
                .expect("Failed to get the home directory!")
                .join(".local/share/fish/fish_history"),
            expected: StringContent::Part("\n- cmd: sudo dnf upgrade"),
        })
        .hint("Did you try to update the system? Are you in the fish shell?")
        .build()
        .into_task("system-update")
}
