mod collective_score_intro;
mod cowsay;
mod initial_house;
mod lolcat;
mod passwd;
mod system_update;
mod zombie;
mod zombie_nuked;

use collective_score_client::check::Task;

pub fn tasks() -> [Task; 8] {
    [
        collective_score_intro::task(),
        initial_house::task(),
        zombie::task(),
        zombie_nuked::task(),
        passwd::task(),
        system_update::task(),
        cowsay::task(),
        lolcat::task(),
    ]
}
