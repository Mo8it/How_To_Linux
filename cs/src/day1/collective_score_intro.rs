use collective_score_client::{
    check::{Check, IntoTask, Task},
    validator::{stdin::Stdin, string_content::StringContent},
};

pub fn task() -> Task {
    Check::builder()
        .description("Checking stdin")
        .validator(Stdin {
            expected: StringContent::Full("OK\n"),
        })
        .hint("Did you pipe `echo \"OK\"` into this command?")
        .build()
        .into_task("collective-score-intro")
}
