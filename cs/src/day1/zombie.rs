use collective_score_client::{
    check::{Check, IntoTask, Task},
    validator::{
        command::{Command, CommandStatus, StdioVariant},
        file::FileContent,
        string_content::StringContent,
    },
};
use std::path::PathBuf;

pub fn task() -> Task {
    [
        Check::builder()
            .description("Looking for the zombie")
            .validator(FileContent {
                file: PathBuf::from("living_room/zombie.txt"),
                expected: StringContent::Part("Brainzzz"),
            })
            .hint("Did you put a zombie in the living room having `Brainzzz` in his head?")
            .build()
            .into_box(),
        Check::builder()
            .description("Checking that Max ran away")
            .validator(
                CommandStatus::builder()
                    .command(
                        Command::builder()
                            .program("ls")
                            .args(vec!["living_room/Max.txt".into()])
                            .stdout(StdioVariant::Null)
                            .stderr(StdioVariant::Null)
                            .stdin(StdioVariant::Null)
                            .build(),
                    )
                    .status_code(2)
                    .build(),
            )
            .hint("Did you move Max away from the living room? Maybe you cloned Max and left one of his copies in the living room 😯")
            .build()
            .into_box(),
        Check::builder()
            .description("Looking for Max")
            .validator(FileContent {
                file: PathBuf::from("bedroom/Max.txt"),
                expected: StringContent::Part("beatboxing"),
            })
            .hint("Did you move Max into the bedroom? Does he still have his hobby?")
            .build()
            .into_box(),
    ]
    .into_task("zombie")
}
