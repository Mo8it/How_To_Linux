use collective_score_client::{
    check::{Check, IntoTask, Task},
    validator::{file::FileContent, string_content::StringContent},
};
use std::path::PathBuf;

pub fn task() -> Task {
    [
        Check::builder()
            .description("Checking the bedroom")
            .validator(FileContent {
                file: PathBuf::from("bedroom/bed.txt"),
                expected: StringContent::Full(""),
            })
            .hint("Did you create the directory `bedroom` containing the empty file `bed.txt`?")
            .build()
            .into_box(),
        Check::builder()
            .description("Checking the bathroom")
            .validator(FileContent {
                file: PathBuf::from("bathroom/toilet.txt"),
                expected: StringContent::Full(""),
            })
            .build()
            .into_box(),
        Check::builder()
            .description("Checking the living room")
            .validator(FileContent {
                file: PathBuf::from("living_room/couch.txt"),
                expected: StringContent::Full(""),
            })
            .build()
            .into_box(),
        Check::builder()
            .description("Checking the kitchen")
            .validator(FileContent {
                file: PathBuf::from("kitchen/fridge.txt"),
                expected: StringContent::Full(""),
            })
            .build()
            .into_box(),
        Check::builder()
            .description("Checking the dining room")
            .validator(FileContent {
                file: PathBuf::from("dining_room/chair.txt"),
                expected: StringContent::Full(""),
            })
            .build()
            .into_box(),
        Check::builder()
            .description("Looking for Max in the living room")
            .validator(FileContent {
                file: PathBuf::from("living_room/Max.txt"),
                expected: StringContent::Part("beatboxing"),
            })
            .hint("Did you add his hobby `beatboxing` to his file?")
            .build()
            .into_box(),
    ]
    .into_task("initial-house")
}
