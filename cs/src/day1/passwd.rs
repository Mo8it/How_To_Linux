use collective_score_client::{
    check::{Check, IntoTask, Task},
    validator::{file::FileContent, string_content::StringContent},
};

pub fn task() -> Task {
    Check::builder()
        .description("Checking that you changed your password")
        .validator(FileContent {
            file: dirs::home_dir()
                .expect("Failed to get the home directory!")
                .join(".local/share/fish/fish_history"),
            expected: StringContent::Part("- cmd: passwd\n"),
        })
        .hint("Did you change your password by running the command `passwd`? Are you in the fish shell?")
        .build()
        .into_task("passwd")
}
