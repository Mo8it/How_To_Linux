use collective_score_client::{
    check::{Check, IntoTask, Task},
    validator::{
        command::{Command, CommandStatus, StdioVariant},
        file::FileContent,
        string_content::StringContent,
    },
};
use regex::Regex;

pub fn task() -> Task {
    let fish_history = dirs::home_dir()
        .expect("Failed to get the home directory!")
        .join(".local/share/fish/fish_history");

    [
        Check::builder()
            .description("Checking that `cowsay` is installed")
            .validator(
                CommandStatus::builder()
                    .command(
                        Command::builder()
                            .program("which")
                            .args(vec!["cowsay".into()])
                            .stdout(StdioVariant::Null)
                            .stderr(StdioVariant::Null)
                            .stdin(StdioVariant::Null)
                            .build(),
                    )
                    .status_code(0)
                    .build(),
            )
            .hint("Did you install `cowsay` using the `dnf` package manager?")
            .build()
            .into_box(),
        Check::builder()
            .description("Checking that you tried to change the eyes")
            .validator(FileContent {
                file: fish_history.clone(),
                expected: StringContent::Regex(
                    Regex::new(r"\n- cmd: cowsay.* -e .+\n").expect("Failed to build a regex"),
                ),
            })
            .hint("Did you read the help message of `cowsay` to find out how to change the eyes?")
            .build()
            .into_box(),
        Check::builder()
            .description("Checking that you tried to change the tongue")
            .validator(FileContent {
                file: fish_history.clone(),
                expected: StringContent::Regex(
                    Regex::new(r"\n- cmd: cowsay.* -T .+\n").expect("Failed to build a regex"),
                ),
            })
            .hint("Did you read the help message of `cowsay` to find out how to change the tongue?")
            .build()
            .into_box(),
    ]
    .into_task("cowsay")
}
