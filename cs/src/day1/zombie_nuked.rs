use collective_score_client::{
    check::{Check, IntoTask, Task},
    validator::{
        command::{Command, CommandStatus, StdioVariant},
        file::FileContent,
        string_content::StringContent,
    },
};
use std::path::PathBuf;

pub fn task() -> Task {
    [
        Check::builder()
            .description("Checking that the zombie was nuked")
            .validator(
                CommandStatus::builder()
                    .command(
                        Command::builder()
                            .program("ls")
                            .args(vec!["living_room".into()])
                            .stdout(StdioVariant::Null)
                            .stderr(StdioVariant::Null)
                            .stdin(StdioVariant::Null)
                            .build(),
                    )
                    .status_code(2)
                    .build(),
            )
            .hint("Did you destroy the living room with the Zombie in it?")
            .build()
            .into_box(),
        Check::builder()
            .description("Looking for Max")
            .validator(FileContent {
                file: PathBuf::from("bedroom/Max.txt"),
                expected: StringContent::Part("beatboxing"),
            })
            .hint("Is Max in the bedroom? Does he still have his hobby?")
            .build()
            .into_box(),
    ]
    .into_task("zombie-nuked")
}
