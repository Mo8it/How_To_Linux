use collective_score_client::{
    check::{Check, IntoTask, Task},
    validator::{
        command::{Command, CommandStatus, StdioVariant},
        file::FileContent,
        string_content::StringContent,
    },
};

pub fn task() -> Task {
    let fish_history = dirs::home_dir()
        .expect("Failed to get the home directory!")
        .join(".local/share/fish/fish_history");

    [
        Check::builder()
            .description("Checking that `cargo-update` is installed")
            .validator(
                CommandStatus::builder()
                    .command(
                        Command::builder()
                            .program("which")
                            .args(vec!["cargo-install-update".into()])
                            .stdout(StdioVariant::Null)
                            .stderr(StdioVariant::Null)
                            .stdin(StdioVariant::Null)
                            .build(),
                    )
                    .status_code(0)
                    .build(),
            )
            .hint("Did you install `cargo-update` using `cargo`?")
            .build()
            .into_box(),
        Check::builder()
            .description("Checking that `tldr` is installed")
            .validator(
                CommandStatus::builder()
                    .command(
                        Command::builder()
                            .program("which")
                            .args(vec!["tldr".into()])
                            .stdout(StdioVariant::Null)
                            .stderr(StdioVariant::Null)
                            .stdin(StdioVariant::Null)
                            .build(),
                    )
                    .status_code(0)
                    .build(),
            )
            .hint("Did you install `tealdeer` using `cargo`?")
            .build()
            .into_box(),
        Check::builder()
            .description("Checking that you ran tldr")
            .validator(FileContent {
                file: fish_history.clone(),
                expected: StringContent::Part("\n- cmd: tldr dnf\n"),
            })
            .hint("Did check the command `dnf` using `tldr`?")
            .build()
            .into_box(),
        Check::builder()
            .description("Checking that you ran tldr not only once")
            .validator(FileContent {
                file: fish_history.clone(),
                expected: StringContent::Part("\n- cmd: tldr apt\n"),
            })
            .hint("Did check the command `apt` using `tldr`?")
            .build()
            .into_box(),
    ]
    .into_task("cargo")
}
