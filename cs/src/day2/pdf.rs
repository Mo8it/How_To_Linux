use collective_score_client::{
    check::{Check, IntoTask, Task},
    validator::{file::FileContent, string_content::StringContent},
};
use std::path::PathBuf;

pub fn task() -> Task {
    Check::builder()
        .description("Checking the downloaded PDF file")
        .validator(FileContent {
            file: PathBuf::from("knowunity.pdf"),
            expected: StringContent::Part("Build 19E258"),
        })
        .hint("Did you download the PDF file?")
        .build()
        .into_task("pdf")
}
