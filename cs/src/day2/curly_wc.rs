use collective_score_client::{
    check::{Check, IntoTask, Task},
    validator::{stdin::Stdin, string_content::StringContent},
};

pub fn task() -> Task {
    Check::builder()
        .description("Checking the number of characters")
        .validator(Stdin {
            expected: StringContent::Full("756\n"),
        })
        .hint("curl … | … | cs task curly-wc")
        .build()
        .into_task("curly-wc")
}
