use collective_score_client::{
    check::{Check, IntoTask, Task},
    validator::{file::FileContent, string_content::StringContent},
};

pub fn task() -> Task {
    Check::builder()
        .description("Checking you made Fish the default Zellij shell")
        .validator(FileContent {
            file: dirs::home_dir()
                .expect("Failed to get the home directory!")
                .join(".config/zellij/config.kdl"),
            expected: StringContent::Part("default_shell \"/usr/bin/fish\""),
        })
        .hint(
            "Did you set the option `default_shell`? Did you check the path of fish using `which`?",
        )
        .build()
        .into_task("fish-in-zellij")
}
