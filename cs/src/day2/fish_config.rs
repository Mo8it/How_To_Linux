use collective_score_client::{
    check::{Check, IntoTask, Task},
    validator::{file::FileContent, string_content::StringContent},
};

pub fn task() -> Task {
    Check::builder()
        .description("Checking you disabled the Fish greeting")
        .validator(FileContent {
            file: dirs::home_dir()
                .expect("Failed to get the home directory!")
                .join(".config/fish/config.fish"),
            expected: StringContent::Part("set -g fish_greeting"),
        })
        .build()
        .into_task("fish-config")
}
