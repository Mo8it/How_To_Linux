use collective_score_client::{
    check::{Check, IntoTask, Task},
    validator::{file::FileContent, string_content::StringContent},
};
use std::path::PathBuf;

pub fn task() -> Task {
    [
        Check::builder()
            .description("Checking the stdout file")
            .validator(FileContent {
                file: PathBuf::from("normal_output.log"),
                expected: StringContent::Full("OK\nThis is some random output\nunshipped hardly lip cactus appetite petticoat\n"),
            })
            .build()
            .into_box(),
        Check::builder()
            .description("Checking the stderr file")
            .validator(FileContent {
                file: PathBuf::from("errors.log"),
                expected: StringContent::Full("Just do what the tasks tells you\nbundle favored sierra ungraded uneaten passage\ncrummy worrisome nearness level stays handmade\n"),
            })
            .build()
            .into_box(),
        Check::builder()
            .description("Checking the mixed file")
            .validator(FileContent {
                file: PathBuf::from("verbose.log"),
                expected: StringContent::Full("OK\nThis is some random output\nJust do what the tasks tells you\nbundle favored sierra ungraded uneaten passage\nunshipped hardly lip cactus appetite petticoat\ncrummy worrisome nearness level stays handmade\n"),
            })
            .build()
            .into_box(),
        Check::builder()
            .description("Checking the appended stdout")
            .validator(FileContent {
                file: PathBuf::from("dont_overwrite.txt"),
                expected: StringContent::Full("spherical survey capillary relatable tameness fame\nOK\nThis is some random output\nunshipped hardly lip cactus appetite petticoat\n"),
            })
            .build()
            .into_box(),
    ]
    .into_task("redirections")
}
