mod cargo;
mod curly_wc;
mod fish_config;
mod fish_in_zellij;
mod pdf;
mod redirections;

use collective_score_client::check::Task;

pub fn tasks() -> [Task; 6] {
    [
        cargo::task(),
        fish_config::task(),
        fish_in_zellij::task(),
        curly_wc::task(),
        redirections::task(),
        pdf::task(),
    ]
}
