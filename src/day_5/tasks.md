# Tasks

## Task: SSH 🔑

Generate an SSH key pair in the browser terminal if you did not do so on day 3 yet.
Send me the public key per email: mo8it@proton.me

The public key has the extension `.pub`.
Don't send me the private key!!!
**You should never send your private SSH keys to anyone!**

I will then append your public key to `~/.ssh/authorized_keys` on the server that we will use in the next tasks.
After I add your public key, you will be able to login to the server and do the next tasks.

Create the file `~/.ssh/config` if it does not exist and add the server as a host with the name `linux-lab`:

```
Host linux-lab
  HostName 38.242.215.155
  User admin
```

After that I add your public key, connect to the server using the host name that you entered in `~/.ssh/config` which is `linux-lab`:

```bash
ssh linux-lab
```

## Task: User creation 👤

1. Create a user for yourself on the server after connecting with SSH. To do so, run:
   ```bash
   sudo useradd USERNAME
   ```

   Replace `USERNAME` with your name.
1. Now, set a password for the new user:

   ```bash
   sudo passwd USERNAME
   ```
1. For the new user to be able to use `sudo`, it has to be added to the `wheel` group:

   ```bash
   sudo usermod -aG wheel USERNAME
   ```

   `-aG` stands for _append to group(s)_.

   (On Debian based distros, the user should be added to the `sudo` group instead of `wheel`.)
1. Now, change your user to the new user:

   ```bash
   su --login USERNAME
   ```

   You will be asked for the password.
   After a successful authentication, you will see that the username changed in the prompt.
1. Run the following command for verification:

   ```bash
   whoami
   ```

   It should not output "admin"!

   Yes, the command is called `whoami`.
   Linux can be philosophical sometimes 🤔
1. Now, verify that you can run `sudo` as the new user:

   ```bash
   sudo whoami
   ```

   You should see "root" as output because `sudo` runs a command temporarily as the `root` user.
1. `cd` to the home directory of the new user.
1. Make sure that you are in the home directory of the new user! Run `pwd` to verify that you are **NOT** in `/home/admin` ⚠️ **PLEASE, DON'T TOUCH `/home/admin/.ssh`** ⚠️ Now, create the directory `~/.ssh` in the home directory of the new user. Change the permissions of `~/.ssh` such that only the user has read, write and execution permissions. _group_ and _others_ should have no permissions for `~/.ssh`!
1. Create the file `authorized_keys` inside `~/.ssh`. Only the user should have read and write permissions for the file. _group_ and _others_ should have no permissions for it!
1. Copy the content of your public key file (with the extension `.pub`) to this file. It should be one line! Then save the file.
1. Logout from the server to get back to the system in the **browser terminal**. Go to `~/.ssh/config` that you edited at the beginning of this task. Change the user for the host `linux-lab` from `admin` to `USERNAME` where `USERNAME` is the name of the new user that you created on the server.
1. Try to connect using the host name again. If you did everything right, you should be connected and be the user that you did create. Run `whoami` to verify that the output is not "admin".

## Task: File transfer ⬆️⬇️

In the system of the browser terminal, use `rsync` to upload some files and directories that you created during the course to the server `linux-lab` ⬆️

Now, login to the server with SSH to verify that the files and directories were uploaded correctly.

While on the server, create a file with some text in it.
Remember its path!

Now, logout from the server and use `rsync` to download that file to your system ⬇️

## Task: Compilation in containers 📦️

> 📍 : This task should be done on the server using the user that you created (**not** admin).

We want to practice scripting and dealing with containers.
Therefore, we will compile something in a container!

We want to compile the program `tmate`:

1. Start Zellij on the system of the browser terminal.
1. Login the user that you created on the server (**not** admin).
1. Start an Ubuntu container with `podman run -it --rm --name tmate-compiler ubuntu:latest bash`.
1. Run `apt update` to be able to install packages with the `apt` package manager in the next steps.
1. Go to the [website of `tmate`](https://tmate.io/) and find out how to compile from **source** (there are instructions for compiling on Ubuntu).
1. Install the packages that are required for the compilation with `apt install`. These packages are listed on the website where the compilation instructions are.
1. Follow the actual compilation instructions on the website. The compilation might take some minutes.
1. After compilation, you will find the program file `tmate` in the directory of the git repository.
1. Don't exit the container yet, otherwise you will lose what you have done in it! Now, open a new Zellij pane, login to the same user on the server and copy the binary `tmate` from the container to the directory `bin` in your home directory. Use the command `podman cp CONTAINERNAME:SRC_PATH DESTINATION_PATH`.
1. Verify that the binary `tmate` was copied to `DESTINATION_PATH` and then exit the container in the first Zellij pane.

Now, write a script called `compile_tmate.sh` that automates what you have done in the container to compile `tmate`.
Just copy all the commands that you used inside the container to a script.

Add `mv PATH_TO_THE_TMATE_PROGRAM_IN_THE_CONTAINER /volumes/bin` to the end of the script to move the binary to the directory `/volumes/bin` after compilation.

Create a directory called `scripts` and put the script in it.

Now, write a second script in the **parent directory** of the directory `scripts`.
The second script should automate creating the container that runs the first script (`compile_tmate.sh`).

Do the following in the second script:

1. Check if `scripts/compile_tmate.sh` does NOT exist. In that case, print an error message and exit with the code 1.
1. Make sure that `scripts/compile_tmate.sh` is executable for the user.
1. Create a directory called `bin` (next to the directory `scripts`) if it doesn't already exist.
1. Use the following snippet:

   ```bash
   podman run -it --rm \
       --name tmate-compiler \
       --volume ./scripts:/volumes/scripts:Z,ro \
       --volume ./bin:/volumes/bin:Z \
       docker.io/library/ubuntu:latest \
       /volumes/scripts/compile_tmate.sh
   ```

   It creates a container that runs the script `compile_tmate.sh` and is removed afterwards (because of `--rm`).

   The `scripts` directory is mounted as a volume to be able to give the container access to the script `compile_tmate.sh`.
   It is mounted as _read only_ (`ro`) because it will not be modified.

   The `bin` directory is mounted to be able to transfer the binary into it before the container exits.

After running the second script, you should see the container compiling and then exiting. At the end, you should find the binary `tmate` in the `bin` directory.

Now, that you have the program `tmate`, find out what it does!
Try it with another participant 😃

### Hints

- On Debian based distributions like Ubuntu, the package manager is `apt`. Before that you can install any packages with `apt`, you have to run `apt update`. This does not run system updates like `dnf upgrade`. `apt update` does only synchronize repositories which is needed before any installation.
- You can use the following snippet to test if a file exists in bash:

  ```bash
  if [ -f FILE_PATH ]
  then
    …
  fi
  ```

  Replace `…` with your code.

  For more information on the option `-f` and other useful options for bash conditions, read the man page of the program `test` inside of `bash`: `man test`.

  To test if a file does NOT exist, replace `-f` with `! -f`.
- You can exit a Bash script returning an exit code using `exit`:
  ```bash
  exit 1
  ```

## Task: Static website 🌐

> 📍 : In this task, you should connect as the user `admin` to the server. **Don't do this task as the user that you created on the server!** ⚠️ Just run `ssh admin@linux-lab`

> 📍 : Starting with this task: Asking you to replace `N` means to enter the number that you are using in the URL `ttydN.mo8it.com`.

In this task, you will host a static website.
A static website is just a set of HTML, CSS (and optionally JavaScript) files (no backend).

To host the website, we need a web server.
In this task, we will use the Nginx web server.

Create the directory `~/nginxN` after replacing `N`.
Create two directories inside it: `website` and `config`.

Place these two files:

1. `~/nginxN/config/nginx.conf` (replace `N`):

   ```
   server {
       root /volumes/website;
       index index.html;

       location / {
       }
   }
   ```
1. `~/nginxN/website/index.html` (replace `N`):

   ```
   <!doctype html>
   <html lang="en">
     <head>
       <meta charset="UTF-8">
       <meta name="viewport" content="width=device-width, initial-scale=1.0">
       <title>Demo</title>
     </head>
     <body>
       <h1>Hello world!</h1>
     </body>
   </html>
   ```

Create an Nginx container with the following options:

- Name: `nginxN`. Replace `N`!
- Network: `traefik`.
- Volumes:
  - `~/nginxN/website:/volumes/website` with labels `Z,ro`.
  - `~/nginxN/config:/etc/nginx/conf.d` with labels `Z,ro`.
- Image: `docker.io/library/nginx:latest`

Create the systemd service file for the container above.

Move the systemd service file to `~/.config/systemd/user`.

Enable and start the container as a user services with `systemctl --user enable --now container-nginxN`.
Replace `N`!

Visit `https://nginxN.mo8it.xyz` in your browser to see if everything did work!
Replace `N`!

Now, you can edit `index.html` and add your own HTML content.

You can also add more files to the directory `website`.
If you add a file `test.html` for example, then you should see it under the link `https://nginxN.mo8it.xyz/test`.

## Task: Nextcloud ☁️

> 📍 : In this task, you should connect as the user `admin` to the server. **Don't do this task as the user that you created on the server!** ⚠️ Just run `ssh admin@linux-lab`

In this task, you will deploy your own cloud on the server: [Nextcloud](https://nextcloud.com/)!

To do so, we will install Nextcloud as a container using `podman`.

You can find more information about the Nextcloud container [here](https://hub.docker.com/_/nextcloud).

Create the directory `~/nextcloudN` (replace `N`).

Create a directory called `~nextcloudN-db` (replace `N`) for the database container.

Create a container for the database with the following options:

- Container name: `nextcloudN-db`. Replace `N`!
- Network: `traefik`
- Volume: Mount the directory `nextcloudN-db` (replace `N`) that you created into `/var/lib/postgresql/data` in the container. Use the label `Z`!
- The following environment variables:
  - `POSTGRES_DB=nextcloud`
  - `POSTGRES_USER=nextcloud`
  - `POSTGRES_PASSWORD=DB_PASSWORD`. Replace `DB_PASSWORD` with a good password!
- Image: `docker.io/library/postgres:16`

Create the actual Nextcloud container with the following options:

- Container name: `nextcloudN`. replace `N`!
- Network: `traefik`
- Volume: Mount the directory `nextcloudN` that you created into `/var/www/html` in the container. Use the label `Z`!
- The same environment variables as for the other container! Use the same `DB_PASSWORD`. Add one more environment variable:
  - `POSTGRES_HOST=nextcloudN-db`. Replace `N`!
- Image: `docker.io/library/nextcloud:27-apache`

Create the systemd files for both containers.
Move the systemd files to `~/.config/systemd/user`.

Enable and start both containers as a user services with `systemctl --user enable --now container-nextcloudN-db` and `systemctl --user enable --now container-nextcloudN`.
Replace `N`!

Visit `https://nextcloudN.mo8it.xyz` to see if everything did work!
Replace `N`!
