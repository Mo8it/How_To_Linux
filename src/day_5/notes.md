# Notes

## SSH

### Setup host

In `~/.ssh/config`

```
Host HOST
    HostName SERVERIP
    User SERVERUSER
```

### Generate key pair

```bash
ssh-keygen -t ed25519 -C "COMMENT"
```

Leave blank to take default for the prompt `Enter file in which to save the key (/home/USERNAME/.ssh/id_ed25519)`.

Then, you can enter a passphrase for your key.
To keep it simple while learning, we will not add one now.
Press enter twice to not use a passphrase.
**But you should use a passphrase when you work with real servers!**

### Add the public key to the server

```bash
ssh-copy-id -i ~/.ssh/id_ed25519.pub HOST
```

### Connect

```bash
ssh HOST
```

### Config on server

**Very important for security!** Only after adding the public key to the server!

> WARNING ⚠️ :
>
> Verify that you are only asked for the passphrase of the SSH key before continuing in this section!
>
> If you are asked for the password of the user on the server when connecting, then the authentication with a key did not work. Therefore, don't set `PasswordAuthentication no`! Fix the issue with the key authentication first. **Otherwise, you will be locked out of the server!** ⚠️

In `/etc/ssh/sshd_config` on the server:

Uncomment line with `PasswordAuthentication` and set it to `PasswordAuthentication no`

Save and exit, then run:

```bash
sudo systemctl restart sshd
```

If you are locked out after running this command, then you did not take the warning above seriously!

## Rsync

From server:

```bash
rsync -Lahz HOST:SRC_PATH DEST_PATH
```

To server:

```bash
rsync -Lahz SRC_PATH HOST:DEST_PATH
```

Options:

- `-a`, `--archive`: Set of useful options to preserve permissions, use recursive mode, etc.
- `-h`, `--human-readable`: Output number in a human-readable format.
- `-z`, `--compress`: Use compression.
- `--partial`: Continue after interruption.
- `-L`, `--copy-links`: Copy links.
- `-v`, `--verbose`: Show more infos.
- `--delete`: Delete files from `DEST_PATH` if they don't exist on `SRC_PATH` anymore. **Use with caution!!!**

## Systemd

Check status of a service:

```bash
sudo systemctl status SERVICENAME
```

Enable service:

```bash
sudo systemctl enable SERVICENAME
```

Start service:

```bash
sudo systemctl start SERVICENAME
```

Enable and start service at the same time:

```bash
sudo systemctl enable --now SERVICENAME
```

Disable service:

```bash
sudo systemctl disable SERVICENAME
```

Stop service:

```bash
sudo systemctl stop SERVICENAME
```

Disable and stop service at the same time:

```bash
sudo systemctl disable --now SERVICENAME
```

## Firewalld

Install and enable firewalld:

```bash
sudo dnf install firewalld
sudo systemctl enable --now firewalld
```

View open ports and services:

```bash
sudo firewall-cmd --list-all
```

Open ports 80 (http) and 443 (https):

```bash
sudo firewall-cmd --add-service http
sudo firewall-cmd --add-service https
sudo firewall-cmd --runtime-to-permanent
```

or:

```bash
sudo firewall-cmd --add-port 80/tcp
sudo firewall-cmd --add-port 443/tcp
sudo firewall-cmd --runtime-to-permanent
```

## Podman

```bash
# Search for image
podman search python

# Pull image
podman pull docker.io/library/python:latest

# See pulled images
podman images

# Run container and remove it afterwards
podman run -it --rm docker.io/library/python:latest bash

# Create network
podman network create NETWORKNAME

# Create container
podman create \
    --name CONTAINERNAME \
    --network NETWORKNAME \
    -e ENVVAR="Some value for the demo environment variable" \
    --tz local \
    docker.io/library/python:latest

# Start container
podman start CONTAINERNAME

# Enter a running container
podman exec -it CONTAINERNAME bash

# Stop container
podman stop CONTAINERNAME

# Generate systemd files
podman generate systemd --new --files --name CONTAINERNAME

# Create directory for user's systemd services
mkdir -p ~/.config/systemd/user

# Place service file
mv container-CONTAINERNAME.service ~/.config/systemd/user

# Activate user's service (container)
systemctl --user enable --now container-CONTAINERNAME
```

Keep user's systemd services live after logging out:

```bash
sudo loginctl enable-linger USERNAME
```

Options:

- `-v`, `--volume`: `SRC_PATH:DEST_PATH:L`. Label should be one of `z`, `z,ro`, `Z` or `Z,ro`.
- `-p`, `--publish`: `SERVER_PORT:CONTAINER_PORT`
