# Dev Tools

_by Mo Bitar (@mo8it)_

So you want to be a developer and you wonder what hard[^hard] skills you should learn besides programming?
Then this course is for you!

This course will not teach you how to program.
It will not teach you any algorithms or data structures.
It is designed to teach you skills that almost every developer needs but are not taught in lectures or the like.

It all starts with the terminal.

You can try to run away from the terminal and look for a [GUI](https://en.wikipedia.org/wiki/Graphical_user_interface) (graphical user interface) for every task.
But learning how to work in the terminal will unlock powerful capabilities as a developer and bring you to a next level of productivity.
Not only will you be able to host and administrate servers (since they usually don't have a GUI).
You will also be able to automate tasks and glue programs together like a wizard 🪄

When we talk about about the terminal for developers, we of course mean the [Linux](https://en.wikipedia.org/wiki/Linux) terminal 🐧

Windows also has a "terminal", but it is so useless that Microsoft itself started to offer a Linux system inside Windows[^wsl].
MacOS being a [Unix-like](https://en.wikipedia.org/wiki/Unix-like) operating system offers a terminal a bit similar to that in Linux, but it is also useless in the server world.

After getting familiar with the Linux terminal, you will learn how to use one of the most important tools for developers: Git

Git is a [version control system](https://en.wikipedia.org/wiki/Version_control).
You need to be familiar with its basics not only to collaborate with others, but also to manage your own software projects with a proper versioning and history.

Afterwards, you learn how to automate tasks using shell scripts and [containers](https://en.wikipedia.org/wiki/Containerization_(computing)).

This course is a 5 days course[^course] and should be read from the start to the end.
If you skip some sections, you might not understand everything in later sections.

The course contains tasks to help you practice and experiment with what you have learned.

Are you excited?
Let's start the journey!

---

[^hard]: Hard skills as the opposite of soft skills; not hard as in difficulty.

[^wsl]: See [Windows subsystem for Linux](https://learn.microsoft.com/en-us/windows/wsl/) which actually should be called "Linux subsystem for Windows".

[^course]: The course started as a vacation course on the Johannes Gutenberg university in Mainz, Germany.
