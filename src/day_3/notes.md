# Notes

The participants of the course will add a description to each of these Git subcommands.

## git config

Configure user-specific information like name, email etc. An example for setting the email is

```bash
git config --global user.email you@gotthis.com
```

An example for setting the username would be

```bash
git config --global user.name "Wonderful-Person"
```

## git init

## git clone

## git status

## git add

This command stages one or more files for the next commit, adding them to everything that is committed.

Examples are:

```bash
git add "example.txt"
```

or

```bash
git add --all
```

if you want to stage all changes.

## git commit

This command is used to record the changes you've made to the repository as a new commit. A commit is a snapshot of the changes you've staged (using `git add`) and is accompanied by a commit message that describes the purpose of the changes you are committing.

Usage of `git commit`:

```bash
git commit -m "Your commit message here"
```

## git log

## git diff

## git branch

## git checkout

This command enables you to switch to another branch or to another commit.

Examples are:

```bash
git checkout "branchname"
```

or

```bash
git checkout -b "branchname"
```

If you want to create a new branch and switch to it.

## git push

## git pull

## git rebase

## git merge

This command is used to combine two branches together. Use git checkout to visit the branch you want to merge to.

```bash
$ git checkout main
$ git merge branch2
```

This will merge the branch "branch2" into the branch, where this command was executed in.(Here: "main")

## git reset

## git revert


## git cherry-pick

### Description

The `git cherry-pick` command applies a specific commit from one branch to another. This can be useful for undoing changes, applying bug fixes to a different branch, or picking out specific changes from a merge commit.

### Usage

The basic syntax for the `git cherry-pick` command is:

```bash
git cherry-pick <commit-hash>
```
where `<commit-hash>` is the SHA-1 hash of the commit you want to cherry-pick. You can also use the `-n` option to preview the changes without actually applying them.

For example, to cherry-pick the commit with the SHA-1 hash `1234567890abcdef`, you would run the following command:

```bash
git cherry-pick 1234567890abcdef
```

Example with visualization of the git log graph:

- Let's assume the current branch state:
```bash
    a - b - c - d   Main
         \
           e - f - g Feature
```

- Ensure that we'are working in the `main` branch:
```bash
git checkout main
```

- Then let's execute the following `cherry-pick` command:
```bash
git cherry-pick f
```

- Once executed, branch state will change to:
```bash
    a - b - c - d - f   Main
         \
           e - f - g Feature
```

### Options

The `git cherry-pick` command has a few options that you can use to customize its behavior:

* `-n`: Preview the changes without actually applying them.
* `-e`: Edit the commit message before applying it.
* `-s`: Sign the commit with your GPG key.
* `-S[<keyid>]`: Sign the commit with the specified GPG key.


## git remote

## git blame

## git stash

## git tag

## git lfs
