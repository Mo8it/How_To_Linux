# Tasks

## Task: Installation

Install Git and [VS-Code](https://code.visualstudio.com/) on your own machine (not in the provided browser terminal).

Open a Git terminal and run the following commands to tell Git about your name (`USERNAME`) and email (`EMAIL`)

```bash
$ git config --global user.name "USERNAME"

$ git config --global user.email "EMAIL"
```

You don't have to use VS-Code for the tasks, but you should at least try it out with its Git integration.

## Task: Register on Codeberg 🏔️

Register an account on [Codeberg](https://codeberg.org/).
It is a Git platform similar to Github and Gitlab, but it is fully free and open source and based in Europe.

## Task: Stars ⭐️

_Just to learn_ how to star repositories, you could add some stars to [my personal ones](https://codeberg.org/mo8it) 😜

We do everything here for educational purposes 😇

## Task: Issues

Create an issue on [the repository of the course](https://codeberg.org/mo8it/dev-tools) that gives me some quick feedback about the course so far.

Read some of the issues created by others and add some emoji reactions and maybe a reply to them.

## Task: SSH key pair 🔑

Create an SSH key pair _without a passphrase_ and add its public part to your Codeberg account following the beginning of [this guide](https://docs.codeberg.org/security/ssh-key/).

I am recommending to not use a passphrase for now to be able to focus on Git itself.
But for a better security in general, you should use a passphrase for your SSH keys outside this course.

## Task: Markdown

You can take a look at [this guide](https://docs.codeberg.org/getting-started/first-repository/) for this task.

Create a new repository called `dev-tools-demo` and clone it to your machine with `git clone SSH_GIT_LINK`.

After cloning, a new directory should be created with the name of the repository.
Move into that directory and create the markdown file `README.md`.

Follow [this guide about Markdown](https://www.markdownguide.org/basic-syntax/) to write some words about the course so far in the README file.
Use all of the Markdown following building blocks:

- Headings with different levels
- Paragraphs with some **bold** and _italic_ text
- A list
- A table
- A blockquote
- Some `bash` code examples inside of code blocks
- A link to some website
- An image

Create a commit after each building block:

```console
$ git add README.md

$ git commit -m "MEANINGFUL MESSAGE"
```

`push` your changes after being done with all building blocks.

Verify that the README file was rendered as expected.
If not, fix the Markdown issues, commit and push the changes again.

## Task: License 📄

Add the [AGPLv3 license](https://choosealicense.com/licenses/agpl-3.0/) as a file `LICENSE.txt` to your demo repository.
Commit and push the file!

You can read more about licenses later.
Discussing different ones is out of the scope of this course.

But it is important to add a license to your projects!
AGPLv3 is the one that I normally use and recommend for free open source software.
You can read about it and compare it to others using [choosealicense.com](https://choosealicense.com) for example.

## Task: Write the script for me 😜

Fork [the repository of the course](https://codeberg.org/mo8it/dev-tools) and clone your fork into your machine.

You will find the most important Git subcommands in the file `src/day_3/notes.md`, but these are still missing a description.

Pick one of the subcommand that don't have a description yet.
Create a branch that has the same name as the chosen Git subcommand.

Add a description to the chosen subcommand in your own words.
Add some examples for how to use that subcommand.

Commit your changes and push the branch to your fork.
Create a pull request using that branch.

## Task: Collaboration

> Note: You have to skip this task if you are working from home.

Team up with another participant that is also done with his demo repository and do the following together:

- Give the other participant a star ⭐️
- On the repository of the other participant, create an issue giving some **nice** feedback about the content of the README file.
- Reply to the issue created by the other participant on your own repository and close it if no actions are required.
- Fork the other repository, clone the fork, create a new branch and add a few lines to the README file that fit the original content.
- Push that branch to your fork.
- Create a pull request on the original repository using the pushed branch.
- Merge the pull request that you receive on your own repository.

## Task: Oh My Git!

Download and play the awesome educational game [Oh My Git!](https://ohmygit.org/) 🎮️
