# CLIs of the day

## [typos](https://github.com/crate-ci/typos)

A command line tool that checks your (code) files for typos.

You can just run it in the directory that you want to check and it will show you all the typos it finds.

You can then run `typos -w` to let it fix typos automatically.
But you have to fix some typos yourself if `typos` has more than one possible fix.

## [gitui](https://github.com/extrawurst/gitui)

A terminal user interface (TUI) for Git written in Rust 🦀
It is very user friendly and self explanatory like Zellij.

## [lazygit](https://github.com/jesseduffield/lazygit)

Another TUI for Git (but written in Go 👎️).
Try both to find out which one you like more.

## [git-cliff](https://git-cliff.org/)

Automatically generates a `CHANGELOG.md` file based on [conventional commits](https://www.conventionalcommits.org/en/v1.0.0/).

Conventional commits have their pros and cons.
Try them out to see if you like their systematic approach.
