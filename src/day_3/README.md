# Day 3

This day is all about Git and it will be very interactive.

I will present the content live while drawing on the board, running commands, using graphical user interfaces and git platforms.

If you are reading this from home, then I am sorry for not having explanations here.
I think that Git can be explained better live.
But don't worry, if you do the tasks then you will have a solid knowledge about Git.
Especially if you finish big parts of the game [Oh My Git!](https://ohmygit.org/)
