# Terminal upgrade

## Fish

When you run something in the terminal, then you are interacting with a so called _shell_.

The default shell on almost all Linux systems is `bash`.
Therefore, you should be familiar with it.

But if you want a modern terminal experience instead of that of the 90s, then you should use the [Fish shell](https://fishshell.com/) 🐠
The **f**riendly **i**nteractive **sh**ell".

Bash offers basic (auto)completion, but Fish takes it to the next level!

Type `ls ~/` (without hitting `Enter` yet) and press `Tab` twice in Bash.
Bash will just show you all possible completion options.
But Fish will let you cycle through these options with `Tab` and `Shift + Tab` to choose one!
This doesn't only work with paths, but also for commands and even command options!

In Bash, you you can cycle through your command history using the up and down arrow keys.
But in Fish, you can also start a command and _then_ cycle through your history that has the same **prefix** with the up and down arrow keys!

It will also automatically give you suggestions to what you type based on history and completion!
These autosuggestions are showed as dimmed text to the right of your input.
To use that suggestion, you can just press the right arrow key (or `Ctrl + e`)!

Fish also supports true color synatx highlighting 🌈

Colors are not only fancy, but they can also be very helpful!
If you start typing a command with a program that does not exit (like `echoo` instead of `echo` for example), Fish will highlight that program in red!
Otherwise, the program is highlighted in blue!
This gives you rapid feedback while typing 🤩

You can also configure Fish in the file `~/.config/fish/config.fish`.
Here is any example

```bash
if status is-interactive
    # Disable the greeting message.
    set -g fish_greeting

    # Abbreviations

    # Prevent overriding files/directories by accident when using `cp` and `mv`.
    abbr -ag cp "cp -i"
    abbr -ag mv "mv -i"
    # Use `trash` by default instead of `rm`.
    abbr -ag rm "trash"
    # Use another shortcut for when you really want to delete instead of moving into a trash.
    abbr -ag rmr "rm -r"
    # Set some default options.
    abbr -ag rsync "rsync -Lahz"

    # Alias
    # Use `bat` instead of `cat` for colored output!
    alias cat "bat"

    # Functions
    function demo
        echo "Hallo from the demo function!"
        echo "Arguments taken: $argv"
        echo "First argument: $argv[1]"
    end
    # Well will learn more about arguments later.
end
```

Use **abbreviations** when you want your input to be replaced while typing with the ability to modify the replacement.
**Aliases** should only be used if you are sure that the two commands are equivalent for you.

## Zellij

Now that we have a fancy shell, what can be even better than a fancy terminal?

Multiple fancy terminals!

Let me introduce you to [Zellij](https://zellij.dev/about/), a modern [terminal multiplexer](https://en.wikipedia.org/wiki/Terminal_multiplexer) written in Rust 🦀

It offers panes, tabs and even floating panes!

Start Zellij by running `zellij` and take a look at the shortcuts at the bottom.
It is pretty user friendly!

Press `Ctrl + p` to go into the _pane mode_ where you can select one of the shortcuts that newly appeared at the bottom.
Press `n` in the _pane mode_ to create a **new pane**!

You can change the focused pane by clicking on another pane.
But there are also keyboard shortcuts for that.

Normally, you can press `Ctrl + t` to go into the _tab mode_ where you can press `n` to create a new tab.
But if you are using the terminal in the browser provided during the course, then you might not be able to press `Ctrl + t` without opening a _browser_ new tab.

Therefore, we have to change that keybinding to something that does not conflict with the browser.
To do so, create the directory `~/.config/zellij` and place the file `config.kdl` in it with the following content:

```kdl
keybinds {
    normal {
        bind "Ctrl b" { SwitchToMode "tab"; }
    }
}
```

This configuration uses the shortcut `Ctrl + b` instead of `Ctrl + t` to enter the _tab mode_.

Now, use that new shortcut followed by `n` to create a new tab.
The easiest way to change the focused tab is by clicking on the tab label at the top.
But there are also keyboard shortcuts for that.

Try detaching the session by pressing `Ctrl + o` followed by `d`.
This will lead to exiting Zellij, but your session (open panes and tabs) is not gone!

To attach back to that session, run `zellij attach` or `zellij a` for short.
Fancy, right?
You can just detach a session you working in, do something else and attach back to continue from where you have left it 😃

There is still something annoying:
Everytime you start zellij, you have to start fish afterwards.
Wouldn't it be awesome to have Fish as the default shell in Zellij?

Let's make it the default one!
Add the following line to the beginning of the configuration file `~/.config/zellij/config.kdl`:

```kdl
default_shell "/usr/bin/fish"
```

You can check that this is the path of the program `fish` on your system by running `which fish`.

We are not done with upgrading our terminal workspace.
We will introduce more modern tools later.
But this should be enough for now 🙂
