# Tasks

Organize the files and directories of your tasks in separate directories!

## Task: Cargo 📦️

Use `cargo` to install the following crates:

- `cargo-update`
- `tealdeer`

It might take a long time to compile everything.

`cargo-update` should be installed to be able to run `cargo install-update -a` to update all installed crates.
Try running the command.
But you should not find any updates since you did just install the crates.

The crate `tealdeer` provides you with the program `tldr`.

Run `tldr --update`. Then run the following two commands:

```bash
tldr dnf
tldr apt
```

It should be obvious to you what `tldr` does after you run the commands above and read their output.
Try it with other programs than `dnf` and `apt`!

🟢 Run `cs task cargo`.

## Task: Fish configuration

Disable the default greeting by Fish everytime you start it.

The configuration file should be `~/.config/fish/config.fish`.

🟢 Run `cs task fish-config`.

## Task: Fish in Zellij

Configure Fish as the default Zellij shell.

🟢 Run `cs task fish-in-zellij`.

## Task: Curly line count.

Use `curl` to fetch this file:
[https://codeberg.org/mo8it/collective-score/raw/commit/4ff0cd6f871e4a17a7ecd36d7d01ca7713c11ca1/Cargo.toml](https://codeberg.org/mo8it/collective-score/raw/commit/4ff0cd6f871e4a17a7ecd36d7d01ca7713c11ca1/Cargo.toml)

You don't have to save it to disk!

Now, pipe the output of `curl` into `wc` (with some option) to count the number of characters.

🟢 Pipe the number of characters into `cs task curly-wc`.

### Hints

- `curl … | … | cs task curly-wc`

## Task: IO redirections

I placed a program called `mixed-output-generator` on your system.

If you run it, it will create the file `dont_overwrite.txt` in your current directory and output random text to both stdout and stderr.

First, run it while writing its standard output to the file `normal_output.log` and its standard error to the file `errors.log`.

After that, run it again while writing both standard output and standard error to the same file `verbose.log`.

Now, run it for the last time while _appending_ the standard output to the file `dont_overwrite.txt` that it creates.

🟢 Run `cs task redirections`.

## Task: Gimme that PDF 😈

The following website uses a PDF file but it doesn't let you download it:
[https://knowunity.de/knows/biologie-neurobiologie-1c6a4647-4707-4d1b-8ffb-e7a750582921](https://knowunity.de/knows/biologie-neurobiologie-1c6a4647-4707-4d1b-8ffb-e7a750582921)

Now that you are kind of a "hacker", you want to use a workaround 😈

Use `curl`, pipes `|`, `rg` (ripgrep) and `xargs` to parse the HTML of the web page, extract the link to the PDF file and download it.
Save the PDF file using the name `knowunity.pdf`.

The link to the PDF file starts with `https://` and ends with `.pdf`.

🟢 Run `cs task pdf` in the same directory where the downloaded PDF file is.

### Hints

- To capture a regex group using ripgrep, you should use `rg '.*(GROUP_PATTERN).*' -r '$1'` after replacing `GROUP_PATTERN` with the pattern that you are looking for.
- If you find the link of the file using `rg`, copy it into a browser to make sure that you got a correct link to a PDF file.

## Task: Vim game

In this task, we are going to play a game! 🎮️

The game is an educational game that lets you learn the very basics of navigation in Vim which we learn about later.
But for now, I can get familiar with the basics during the game.

Play the game on this website: [https://vim-adventures.com](https://vim-adventures.com/)
