# Day 2

In this day, we will learn more about the terminal and how to glue commands together to benefit from their modularity.

But we will start by making our terminal more comfortable!
Let's make an upgrade away from the terminal of the 90s ✨
