# CLIs of the day

## cargo

Cargo is the package manager of the programming language Rust.

You don't have program in Rust to use it.
It is listed here because you can use it to install many of the fancy command line programs written in Rust (like `zellij`, `ripgrep`, `fd-find`, `bat`, `tealdeer`, etc.).

You can install a program with cargo using the command:

```bash
cargo install PROGRAMNAME
```

You don't need to run it with `sudo` since it installs programs for the current user only.
It doesn't modify files not belonging to the user.

To update programs installed using `cargo`, you need to have `cargo-update` installed:

```bash
# The package `openssl-devel` is needed to compile `cargo-update`
sudo dnf install openssl-devel

# To be able to run cargo install-update -a
cargo install cargo-update

# Update installed crates
cargo install-update -a
```

## curl

We did already use `curl`, but not yet for downloading.

```bash
# Download a file into the current directory while keeping the default name of the file.
curl -L LINK_TO_FILE -O

# Download a file while giving the path to save the file into
# (notice that we are using small o now, not O)
curl -L LINK_TO_FILE -o PATH
```

`-L` tells `curl` to follow redirections (for example from `http` to `https`).

## xargs

`xargs` uses each line from the standard input (stdin) as an argument to the command specified after it.

Here is an example that shows the content of all files with the extension `.txt` in the current directory.

```bash
ls *.txt | xargs cat
```

If you have the files `file1.txt` and `file2.txt` in the current directory, then the command above is equivalent to just running `cat file1.txt file2.txt`.

## ripgrep

ripgrep is like `grep`, but it offers [many additional features](https://github.com/BurntSushi/ripgrep#why-should-i-use-ripgrep) and has much better performance (+ it is written in Rust 🦀).

Here is an example of how you can use it with regex to catch a group:

```console
$ cat demo.csv
a,b,c
x,y,z
1,2,3

$ rg '.*,(.*),.*' -r '$1' demo.csv
b
y
2
```
