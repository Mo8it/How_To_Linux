# Regex

[**Reg**ular **ex**pressions](https://en.wikipedia.org/wiki/Regular_expression) specify a match patter in a text.

They can be used for example with `grep`, `rg`, `find`, `fd`, `vim`, etc.

Similar expressions are also used for Git (`.gitignore`) and containerization (`.containerignore`) which we will learn about later.

Here are some of the most important building blocks:

- `^`: Start of line
- `$`: End of line
- `()`: Group
- `|`: Alternation
- `[abcd]`: Character set, here `a` until `d`
- `[a-z]`: Character range, here `a` until `z`
- `[^b-h]`: Negated character range, here `b` to `h`
- `.`: Any character
- `.*`: 0 or more characters
- `.+`: 1 or more characters
- `\w`: Letter or number
- `\W`: Neither letter nor number
- `\d`: Digit
- `\D`: Not digit
- `\s`: Whitespace
- `\S`: Not whitespace

Writing regular expressions is not easy, but there are many (online) tools that you can use to test and debug your regex.
