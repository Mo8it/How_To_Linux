# Tasks

## Task: Collective-Score

This course uses [collective-score](https://codeberg.org/mo8it/collective-score) for interactive tasks.
I spent many days programming it for you 😇

As an attendee of the course, you will find the binary `cs` on the Linux system provided to you.
Check it by running `cs --help`.

You should use it to validate that you have done a task correctly.
But first, you need to register yourself to be able to communicate with the Collective-Score server and see your progress on the projector.

Run the following command **after replacing `USERNAME` with your username** (you can choose whatever username you want to be displayed to others):

```bash
cs user register USERNAME
```

If you get any errors, ask the tutor before proceeding with the tasks!

Now, run the following command:

```bash
echo "Test" | cs task collective-score-intro
```

The check that `cs` does should fail.
This is a demonstration of how checking a task fails and how you (hopefully) get a hint.

In general, you will do a task and then run `cs task TASKCODE` to validate your solution.
`TASKCODE` is a code unique to every task/subtask.
For this demo task, the task code is `collective-score-intro`.

🟢 Now, for this demo task to pass the check, run the following command:

```bash
echo "OK" | cs task collective-score-intro
```

Congratulations, you have done your first task 🎉

To see your progress at any time, run the command **`cs progress show`**.

## Task: Building houses 🏠️

In this task, you will build a house with different rooms using (nested) directories.

Start with an empty directory as the house and add directories to it as rooms.

Rooms (represented by directories) contain objects as text files (ending with `.txt`).
The text files for the objects should be empty for now.

The house should have the following 5 rooms:

- `bedroom` containing `bed.txt`
- `bathroom` containing `toilet.txt`
- `living_room` containing `couch.txt`
- `kitchen` containing `fridge.txt`
- `dining_room` containing `chair.txt`

Place Max in the living room by creating a file `Max.txt` in the living room containing his hobby `beatboxing` as text in his file.

Example:

```console
$ cat living_room/Max.txt
He is watching TV.
His hobby is beatboxing.
```

Run the command `tree` to see the structure of the house.

🟢 Run `cs task initial-house` in the house directory.

Zombies are back!
They did get in through _windows_… (You got it?)

Place a zombie in the living room having `Brainzzz` in his file `zombie.txt`.

Max runs out of the living room!
Move him to the bedroom.

🟢 Run `cs task zombie` in the house directory.

Now, destroy the whole living room with the zombie in it.

🟢 Run `cs task zombie-nuked` in the house directory.

### Hints

- If you get lost, use `pwd`.
- If you are looking for an option but you can not remember it, use `--help`.

## Task: Password 🔑

⚠️ Launch the [fish shell](https://fishshell.com/) by running the command `fish` and stay in it for all following tasks.
If you close the terminal and launch it later again, you have to run `fish` again.
We will learn more about the fish shell later.

Use the command `passwd` to reset the password of your user. It is important to have a secure password!

🟢 Run `cs task passwd`.

## Task: System updates

Find out how to update your system with `dnf` and run the updates.

Before you confirm, make sure that you read what packages are updated. Make a screenshot of the changes.

It is possible that you don't find any updates. In this case, you can try it again on another day!

🟢 Run `cs task system-update`.

## Task: Package installation and usage

Install the package `cowsay` and find out how to use it!

Spoiler: You will have to deal with some cows 🐄

Find out how to give a fancy tongue and shinny eyes.

🟢 Run `cs task cowsay`.

Now, install the package `lolcat`.

Generate some wisdom with `cowsay (…)`. Now, run `cowsay (…) | lolcat`.
What did change?
Can you guess how this internally works?

Now, read the help of `lolcat` and find out how to add some randomness.

🟢 Run `cs task lolcat`.
