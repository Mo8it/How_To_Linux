# Packages

So far, we did only use commands that are common in every Linux system.

Now, we want to start exploring additional commands that might not be preinstalled on your system.

Therefore, we need to learn how to install so called _packages_ to get access to additional programs.

### Package managers

In Linux, you don't install programs by using a search engine to find a website, then visit the website and download an installer, then run the installer and click "Next" and "Agree" repeatedly without even reading what you are agreeing to 🤮

This method is too inconvinient and insecure.
You have to trust the website that you visit.
You have to hope that no malware is delivered while you are downloading the installer from the website.
You can get the malware either from the website itself or through a man-in-the-middle attack.
Then the installer might do whatever it wants.
You just click "Next" and suddenly your system shows you (extra) ads or your browser search preferences are overwritten by Yahoo.
Surprise! 🎉

In Linux, you (mainly) install software through the package manager that is shipped with your distribution.
The software is then downloaded from a repository which is a collection of packages that is managed by the distribution.
The distribution managers make sure that no malware is added to their repositories.

A package manager takes care of verifying the downloaded packages before installation.
This is done using some cryptographic methods and prevents man-in-the-middle attacks or installation of packages that were corrupted during the download process.

The package manager does also take care of installing any other packages that you did not specify but are required by the package that you want to install.
These packages are called _dependencies_.

Since this course uses a [Fedora](https://getfedora.org) system for its demonstrations, we will use Fedora's package manager `dnf`.

If you are using another Linux distribution, you might need to replace the commands of `dnf` with `apt` for Debian based distributions for example.

It is important to understand the functionality of package managers.
Then you can transfer this knowledge to other package managers if required.

This applies to some other aspects in Linux too, since _**the** Linux operating system_ does not exist.
There are distributions that bundle a set of software and differ in small ways.

To install a package, you need administrative privileges.
Therefore, we need some `sudo` powers.
`sudo` will be explained in the next section.

Let's install our first package!
To do so, enter `sudo dnf install cmatrix`.
You will be asked for the password of your user.
Type the password and then press `Enter`.
For security reasons, you will not be able to see you password while entering it.
So don't wonder why nothing happens while typing the password.
Just type it and then press `Enter`.

The package manager might need some time to synchronize some data, then it will show you a summary of what it will do and ask you for confirmation.
Type `y` for _yes_ and then press `Enter` for the installation to start.

After the installation is done, you can enter `cmatrix`.
Congratulations, you are a hacker now! 💻🤓

To exit the matrix, press `q`.

What if you don't like the matrix and want to remove it?
You can uninstall packages using `sudo dnf remove PACKAGENAME`.
In this case:
`sudo dnf remove cmatrix`.
You have to confirm again with `y`.

Why do we speak about _packages_ instead of programs when installing software on Linux?
Because packages can contain more than one binary (the actual program) and extra files.
Take a look at [the files that are installed with `cmatrix` on Fedora](https://packages.fedoraproject.org/pkgs/cmatrix/cmatrix/fedora-rawhide.html#files) for example.

> Warning ⚠️ : While installing or uninstalling packages, it is important to take a look at the summary before confirming the action with `y`.
> There is a reason why a confirmation is required.
> Sometimes, some packages depend on packages with older versions than the versions that exist on your machine.
> If you want to install those packages anyway, other packages that depend on the newer versions could break!
>
> Just don't blindly press `y`.
> If the package manager gives you a warning before doing something, enter this warning in a search engine and read about what it means before continuing!

### Sudo

[![](https://imgs.xkcd.com/comics/sandwich.png)](https://xkcd.com/149/)

Let's try to install another package.
But this time without `sudo` at the beginning of the command.

```console
$ dnf install cowsay
Error: This command has to be run with superuser privileges (under the root user on most systems).
```

You can see that you get an error resulted by a lack of privileges for running this command.

Any action that might modify the system needs administrative privileges.
Installing a package (system-wide) is one of these actions.

Sometimes, an action might not modify the system, but a user might be restricted through some permissions to not be able to access a file for example.
In this case, you would also need to use `sudo`.

Linux has a superuser, one user that exists on every Linux system and is able to do anything!
This user is the `root` user.
For security reasons (and also to not do something destructive by a mistake), this user is often locked.
`sudo` allows you as a non root user to run commands as a `root` user.

You are not always allowed to use `sudo`.
If you don't own the machine you are using and just share it with others, then there is a high chance that only administrators of the machine have `sudo` access.

If you don't have access to `sudo` and try to use it, then you get an output like this:

[![](https://imgs.xkcd.com/comics/incident.png)](https://xkcd.com/838/)

> Warning ⚠️ : Use `sudo` with great caution!
> Do not run a command from the internet that you don't understand!
> Especially if it needs to be run with `sudo`!
> RED FLAG! 🔴
>
> Look up a new command before you run it.
>
> Even commands that you know like `rm` can destroy your system if you use them with `sudo` without knowing what they are doing.
>
> You might find a "meme" in the internet that tells you to run something like `sudo rm -rf /`.
> This command would delete EVERYTHING on your machine.
> It is like deleting the `C` and all other drives at the same time on Windows.
>
> Linux assumes that you know what you are doing when you use `sudo`.
> _With great power comes great responsibility!_

### Looking for a package

If you don't exactly know the name of a package you are looking for, then you can use **`dnf search PATTERN`**.
DNF will then return packages that match this pattern in their name or description.

For example, if you search for Julia (a programming language) you get the following results:

```console
$ dnf search julia
============================ Name Exactly Matched: julia ============================
julia.i686 : High-level, high-performance dynamic language for technical computing
julia.x86_64 : High-level, high-performance dynamic language for technical computing
=========================== Name & Summary Matched: julia ===========================
cantor-julia.i686 : julia backend for cantor
cantor-julia.x86_64 : julia backend for cantor
julia-common.noarch : Julia architecture-independent files
julia-devel.i686 : Julia development, debugging and testing files
julia-devel.x86_64 : Julia development, debugging and testing files
julia-doc.noarch : Julia documentation and code examples
perl-DateTime-Calendar-Julian.noarch : Julian Calendar support for DateTime.pm
vim-syntastic-julia.noarch : A syntax checker for julia programming language
============================== Summary Matched: julia ===============================
jday.i686 : A simple command to convert calendar dates to julian dates
jday.x86_64 : A simple command to convert calendar dates to julian dates
perl-Date-JD.noarch : Conversion between flavors of Julian Date
python3-jdcal.noarch : Julian dates from proleptic Gregorian and Julian calendars
```

If you know the name of the program but you don't know the name of the package that contains this program, use **`dnf provides PROGRAMNAME`**.

If you want to install the program `trash` (no joke 🗑️) that provides you with the functionality of a system trash (instead of completely deleting), then you can run this:

```console
$ dnf provides trash
trash-cli-0.21.10.24-2.fc36.noarch : Command line interface to the freedesktop.org trashcan
Repo        : fedora
Matched from:
Filename    : /usr/bin/trash

trash-cli-0.22.4.16-1.fc36.noarch : Command line interface to the freedesktop.org trashcan
Repo        : updates
Matched from:
Filename    : /usr/bin/trash
```

You can see that the name of the package `trash-cli` is not the same as the name of the program/binary `trash`.
Using `provides` makes your life easier while looking for the package to install.

If you did find a package but you want to get more information about it, you can use **`dnf info PACKAGENAME`**:

```console
$ dnf info julia
Available Packages
Name         : julia
Version      : 1.7.3
Release      : 1.fc36
Architecture : i686
Size         : 45 M
Source       : julia-1.7.3-1.fc36.src.rpm
Repository   : updates
Summary      : High-level, high-performance dynamic language for technical computing
URL          : http://julialang.org/
License      : MIT and LGPLv2+ and GPLv2+
Description  : Julia is a high-level, high-performance dynamic programming language
             : for technical computing, with syntax that is familiar to users of
             : other technical computing environments. It provides a sophisticated
             : compiler, distributed parallel execution, numerical accuracy, and an
             : extensive mathematical function library. The library, largely written
             : in Julia itself, also integrates mature, best-of-breed C and Fortran
             : libraries for linear algebra, random number generation, signal processing,
             : and string processing.
             : 
             : This package only contains the essential parts of the Julia environment:
             : the julia executable and the standard library.

Name         : julia
Version      : 1.7.3
Release      : 1.fc36
Architecture : x86_64
(…)
```

You get two results for Julia that seem to be identical.
The difference is the _architecture_.
Normal computers usually have the `x86_64` architecture, so you should look for it.
But it is nice to know that other architectures are also supported.

To run updates on your system, use **`dnf upgrade`**.

For security reasons, it is important to run updates frequently!
A package manager updates the whole system and its programs.
This means that you don't have to update your packages separately.

If you have a problem with a package after an update, then you can try to use an older version.
To do so, refer to the documentation of **`dnf downgrade`**.
