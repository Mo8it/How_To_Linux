# Why learn Linux?

It is important to answer this question before digging deeper into the course.
Why would you want to learn Linux?

In this course, we are referring to Linux as an operating system (not only the kernel).
Linux is the leading operating system when it comes to servers and computer clusters.
Already since November 2017, all supercomputers on the top 500 supercomputers list are running Linux[^top500].

If you want to build anything on the internet (website, cloud services, etc.), you need a server and therefore you need Linux.
If you want to do numerical computation on a high scale or anything related to the high performance computing field, you need Linux.
The cluster Mogon 2 in Mainz (where this course is/was held) is no exception[^mogon].

If you are not into servers and high performance computing, then Linux will still be interesting for you as a developer!

Many of development tools and programs run only on Linux.
Linux provides you with awesome programs (especially CLI) that could make your life easier, not only as a developer, but also as a person that does more than internet browsing and document editing on the computer.
Linux allows you to do exactly what you want efficiently.
With the help of a shell script as an example (you will learn more about it later), you can automate a task that takes time and has to be done frequently.

On top of all of that, Linux is a set of [free open source software](https://www.gnu.org/philosophy/free-sw.html.en).
This means that you can use, read and study the code of your operating system and also send change requests to its developers.
Everything is transparent!
You can modify free software and redistribute it without any problems.
While using Linux, you enjoy the freedoms of free software and control your computer instead of it being controlling you through closed source software that can not be studied.
You know exactly what runs on your hardware!

Linux can also be used as a desktop!
If you are interested in a free open source operating system that respects you as a user and your privacy and provides you with all the benefits mentioned above and more, then look no further!
Example distributions are [Fedora](https://getfedora.org/) and [Mint](https://linuxmint.com/).
Learning how to deal with the command line will make your experience smoother while using a Linux desktop.
You will get a better understanding of the whole system and its tools after this course which will enable you to use a Linux desktop like a pro 😉

In the next chapter, we will actually start our learning journey with the terminal.

---

[^top500]: [https://www.top500.org/statistics/details/osfam/1/](https://www.top500.org/statistics/details/osfam/1/)

[^mogon]: [https://www.top500.org/system/178930/](https://www.top500.org/system/178930/)
