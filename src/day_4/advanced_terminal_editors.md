# Advanced terminal editors

`nano` is fine if you just want to occasionally edit some files in the terminal.

But if you want to be able to edit files more efficiently, then you should consider Vim and Helix.

## Vim

Vim is a very popular _modal_ terminal editor.

**Modal** means that it has multiple editing modes allowing you to use your keyboard not only to type text but also to navigate and manipulate it.

Modal editing allows you to edit files without needing to touch the mouse.
Being able to work without the mouse is not only good for the terminal, but it also allows you to edit code faster because you keep your hands on the keyboard and have a keybinding for everything.

Each mode has its keybindings.
When you just enter Vim, then you are in the **normal mode** which has the following most important keybindings:

- `:q`: Quit (**very important!**)
- `:wq`: Write buffer (save) and exit (**very important!**)
- `:q!`: Quit without saving (**very important!**)
- `j`: Down
- `k`: Up
- `h`: Left
- `l`: Right
- `i`: Insert at left of cursor
- `a`: Insert at right of cursor (append)
- `I`: Insert at beginning of line
- `A`: Append to end of line
- `Esc`: Normal mode
- `w`: Go to beginning of next word
- `b`: Go to beginning of last word
- `e`: Go to end of word
- `gg`: Go to beginning of file
- `G`: Go to end of file
- `0`: Go to beginning of line
- `$`: Go to end of line
- `%`: Go to the other bracket
- `u`: Undo
- `Ctrl+r`: Redo
- `:h`: Help
- `:w`: Write buffer
- `/PATTERN`: Search
- `n`: Next match
- `N`: Previous match
- `*`: Next match of the word under cursor
- `o`: Add line below and enter insert mode
- `O`: Add line above and enter insert mode
- `v`: Start selection
- `V`: Block selection
- `y`: Yank (copy)
- `p`: Paste
- `x`: Delete one character
- `dw`: Delete word
- `dd`: Delete line
- `D`: Delete util end of line
- `cw`: Change word
- `cc`: Change line
- `C`: Change until end of line
- `di(`: Delete inside bracket `(`. Can be used with other brackets and quotation marks.
- `da(`: Same as above but delete around, not inside.
- `ci(`: Change inside bracket `(`. Can be used with other brackets and quotation marks.
- `ca(`: Same as above but delete around, not inside.
- `:%s/OLD/NEW/g`: Substitute `OLD` with `NEW` in the whole file (with regex)
- `:%s/OLD/NEW/gc`: Same as above but ask for confirmation for every substitution
- `:N`: Go line number `N`
- `.`: Repeat last action
- `<` and `>`: Indentation
- `q`: Start recording a macro (followed by macro character)

When you press `i` for example, then you enter the **insert mode** where you can type text.

There is also the **visual mode** which you can enter by pressing `v` in the normal mode.
It allows you to select text first to then act on it.

You can exit the insert or visual mode by pressing the escape key `Esc`.

You might think that it is complicated.
You are right, it is complicated when you start using it.
You need to practice it for some time, but after that, the keybindings become a second nature of editing for you.
You don't think about which keybindings to press anymore.
You just get the muscle memory and can focus on your editing flow.

Should you use Vim?
I would only recommend using it if you are editing some configuration files or small scripts in the terminal (on a server for example).
But for programming, you should use an editor that offers more features that support you while programming.
It should at least have [LSP](https://en.wikipedia.org/wiki/Language_Server_Protocol) (language server protocol) support.

The easiest option to get started with for programming on your own machine is [VS-Code](https://code.visualstudio.com/).
For a more advanced option, the next section will present another modal terminal editor that has even more features out of the box compared with Vim.

So why do we bother learning Vim then?
Although you don't have to use Vim, you should at least learn it and be familiar with its basic keybindings.
Not only to be able to use it when you have to, but also because Vim keybindings are widely used by other programs, especially terminal user interfaces.

Do you remember how we learned to navigate the manual when using `man`?
Well, many of the keybindings that it supports are inspired by Vim!
Examples are exiting with `q` or `:q` and searching with `/`, `n` and `N`.
But you can also navigate the manual using `j`, `k`, `l`, `h` instead of the arrow keys!
If you press `h` in some manual, then you see all keybindings that you can use and you will be surprised by how many are similar to Vim's.

> **Note**: The keybindings presented when pressing `h` are those of `less` which is a so called [terminal pager](https://en.wikipedia.org/wiki/Terminal_pager)
>
> It is used by other programs too, not only `man`.
> You can also use it yourself by piping a command that produces some long output into it.
>
> Try running `curl -s https://dev-tools.mo8it.com` and then `curl -s https://dev-tools.mo8it.com | less` to see the difference.

You can even use Vim keybindings in other editors to boost your editing efficiency! 🚀
In VS-Code for example, you get Vim keybindings by installing a plugin like [this](https://marketplace.visualstudio.com/items?itemName=vscodevim.vim).

> **Note**: Vim is the successor of `vi`.
> If you are using a system that doesn't have `vim` preinstalled and you don't have superuser privileges to install it,
> then you will most probably at least find `vi` installed.

## Helix

Vim is the most popular modal text editor, but it is old.
If you want to have a modal terminal text editor that can support you when coding, then try out Helix!

It is a modern editor written in Rust with awesome features that you can read about on [its website](https://helix-editor.com/).
The most important one is the language server support which gives you completions, code actions, function signatures, language aware code navigation etc.
You can watch [this video](https://www.youtube.com/watch?v=xHebvTGOdH8) for an introduction to it.

Most of its keybindings are similar to these of Vim.
But it works by a selection followed by a verb like `wd` (word delete) instead of a verb followed by selection like `dw` (delete word) in Vim.
This makes editing more intuitive since you can see what will be changed before the change happens.

Personally, I use Helix for all my text editing including programming and working on servers.
Helix combined with Zellij is a powerful workspace where you can do everything efficiently in the terminal.
It has a steep learning curve though.
VS-Code (maybe with Vim keybindings) can often be enough 😉
