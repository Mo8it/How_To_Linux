# CLIs of the day

## cut

Demo file `demo.txt`:

```
here,are,some
comma,separated,values
de mo,file,t x t
```

```bash
# Get the N-th column by using SEP as separator
cut -d SEP -f N FILE
```

Example:

```console
$ cut -d "," -f 1 demo.txt
here
comma
de mo
```

You can also pipe into `cut` instead of specifying `FILE`.

## sed

```bash
# Substitute
sed 's/OLD/NEW/g' FILE

# Delete line that contains PATTERN
sed '/PATTERN/d' FILE
```

Example:

```console
$ sed 's/values/strings/g' demo.txt
here,are,some
comma,separated,strings
de mo,file,t x t

$ sed '/separated/d' demo.txt
here,are,some
de mo,file,t x t
```

You can also pipe into `sed` instead of specifying `FILE`.

When you specify `FILE`, you can use the option `-i` to operate _inplace_. This means that the file is modified directly.

## find

Find everything ending with `.sh` of type file (`f`) using globbing:

```bash
find . -type f -name '*.sh'
```

Using regex:

```bash
find . -type f -regex '.+\.sh'
```
