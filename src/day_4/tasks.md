# Tasks

Don't forget to add a shebang to your scripts and make them executable!

## Task: Essential tools

Write a Bash script that installs the following _essential_ Linux packages for you:

- `cmatrix`
- `cowsay`
- `lolcat`
- `cbonsai`

After the installation is done, the script should let `cowsay` inform the user that the installation was successful.

Test your script!

> **Fun**: Run the command `cbonsai -i -l -t 0.01 -w 1` 🌳

## Task: Current directory

Write a Bash script that calculates the total number of files and directories (together) in the current directory.
For example, if the current directory has two directories and one file, then the number would be `3`.
Store the result into a variable.

The output should be the full path to the current working directory with a message telling about the calculated number.
Example:

```console
$ whereami.sh
You are in the directory /home/student/scripts
There are 3 files and directories in this directory.
```

### Hints

- `ls -1` shows every file or directory in the current directory on a separate line.

## Task: Backup then update

Write a Bash script that does the following:

1. Create the directory `~/installed_packages` if it does not exist. If it exists, it should not through any errors.
1. Stores the output of the command `dnf list --installed` into the file `~/installed_packages/dnf.txt`.
1. Stores the output of the command `cargo install --list` into the file `~/installed_packages/cargo.txt`.
1. Installs system upgrades using `dnf`.
1. Installs Cargo updates.
1. Check if `cowsay` is installed. Only if not, install it first. After that, `cowsay` should inform you that installed packages where logged into the directory `~/installed_packages` and updates were successfully installed.

If any of the steps above fails, the script should stop and not continue.
This can be achieved by adding the following line to the beginning of the script (after the shebang): `set -e`.

## Task: Scripted "Gimme that PDF" 😈

On day 2, you downloaded a PDF given some web page link.
We want to write a script now that is generic over the web page link.

It should ask the user for a web page link and then download the PDF if a PDF link was found on that web page.

The script should exit with the status code 1 while printing an error message to stderr in case no PDF link was found on the web page ❌

Otherwise, it should print a message to stdout that the download was successful ✔️

## Task: Job scheduler

> **Warning** ⚠️ : This task is not an easy one.
> Don't give up quickly and ask for help if you don't get further!

In this task, we want to write our own job scheduler.

Understanding how job schedulers work is important when you are working on a computer cluster.

Computer clusters are shared by many users.
Therefore, running jobs on a cluster has to be scheduled to make sure that the resources are shared probably.

In this task, we will keep it simple.
No aspects of multiple users or any optimizations.

We want to be able to submit a job as a single script (without any dependencies).
The submitted scripts should run one after the another.

We will use the program `inotifywait`.
This program can monitor a directory and notify on changes within this directory.

1. Start Zellij if you are not already using it.
1. Find out which package provides the program `inotifywait` and install it.
1. Read the manual of `inotifywait` for a better understanding of what it does.
1. Find out what events mean in the context of `inotifywait`.
1. Find out how to tell `inotifywait` to keep monitoring a directory and not exit after the first event.
1. Create a new directory called `jobs` to be monitored.
1. Create a new directory called `logs` that will be used later.
1. Run `inotifywait` while telling it to monitor the directory `jobs`. Leave the command running in one Zellij pane and open a second pane to continue the work in.
1. Create a file **outside** of the directory `jobs` and then copy it to the directory `jobs`.
1. Look at the output of `inotifywait` in the first pane after copying the file in to the `jobs` directory.
1. Based on that output, choose an event that you want to listen to with `inotifywait` that tells you when a file is **completely written** to the directory `jobs`. Use the manual to read more about specific events.
1. Find an option that lets you tell `inotifywait` to only notify when the chosen event occurs.
1. Find an option that lets you format the output of the notification of `inotifywait`. Since we only listen on one event and monitor only one directory, an output that shows **only the name of the new file** should be enough.
1. Enter the `inotifywait` command that you have until now in a script. Now, extend it by using a `while` loop that continuously listens on the notifications of `inotifywait`. Use the following snippet after replacing `…`:
   ```bash
   inotifywait … | while read NEW_SCRIPT_NAME
   do
       …
   done
   ```
1. After a notification, the body of the `while` loop should first print the name of the script that was added (`NEW_SCRIPT_NAME`). Then, the new script should be run.
1. Save the standard output and standard error of the script into two separate files in the `logs` directory. If the name of the script is `job.sh` for example, then the output should be in the files `logs/job.sh.out` and `logs/job.sh.err`.

### Hints

- Take care of file permissions.

## Task: Job submitter

In this task, we will write a small script called submitter that lets us submit a job script to the scheduler from the last task.

The submitter should take the path to the job script as a single required argument.

The submitter should then copy the job script to the directory `jobs` while adding the current time and date as a prefix to the new job script name.

Read the manual of the command `date` to know how to get the current time and date in the following format: `2022-08-22T20:00:00+00:00`.

If the name of the job script is `job.sh` for example, the job script should be named `2022-08-22T20:00:00+00:00_job.sh` in the `jobs` directory.

Use variables while writing the script to make it more understandable.

### Hints

- To save the output of a command into a variable, you have to use the following syntax after replacing `…`:
  ```bash
  DATETIME=$(date …)
  ```

## Task: Submit a job

Write a small job script that requires at least 10 seconds to run (can be simulated with the command `sleep 10`).
Submit that job script by using the submitter from the last task.

Make sure that the scheduler is running in one Zellij pane before submitting.

Submit your job script multiple times and take a look at the pane where scheduler is running to make sure that the job scripts are running one after the other.

Verify the redirection of the standard output and standard error in the directory `logs`.
