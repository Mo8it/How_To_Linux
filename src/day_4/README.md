# Day 4

Today, you will learn to write scripts for task automation.

We will focus on shell scripting in Bash.
But at the end, scripting in Python is presented briefly as a more flexible scripting option in case your scripts get complicated.

But before we start writing our first scripts, we will learn two command line editors that offer many more features than `nano` and can make editing files much more efficient.
